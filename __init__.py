bl_info = {
    "name": "Mosaic format",
    "author": "Michael Nischt",
    "version": (0, 0, 1),
    "blender": (2, 80, 0),
    "location": "File > Export",
    "description": "Exporter for the Mosaic format collections",
    "warning": "",
    "wiki_url": "https://gitlab.com/monoid-labs/mosaic/blender-module/wikis/",
    "tracker_url": "https://gitlab.com/monoid-labs/mosaic/blender-module/issues",
    'support': 'COMMUNITY',
    "category": "Import-Export"}

# To support reload properly, try to access a package var, if it's there, reload everything (F8)
if "bpy" in locals():
    import importlib
    if "export_mosaic" in locals():
        importlib.reload(export_mosaic)
#    if "import_mosaic" in locals():
#        importlib.reload(export_mosaic)

import bpy


from . import export_mosaic
#from . import import_mosaic

def menu_func_export(self, context):
    self.layout.operator(export_mosaic.ExportMosaic.bl_idname, text="Mosaic")

classes = (
    export_mosaic.ExportMosaic,
#    import_mosaic.ImportMosaic,
)

def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.TOPBAR_MT_file_export.append(menu_func_export)
#    bpy.types.TOPBAR_MT_file_import.append(menu_func_import)



def unregister():
    bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)
#    bpy.types.TOPBAR_MT_file_import.remove(menu_func_import)

    for cls in classes:
        bpy.utils.unregister_class(cls)


if __name__ == "__main__":
    register()
    bpy.ops.export.mosaic()
